<?php

namespace DecaturVote\DiffDb;

class TextDiff extends \Tlf\BigOrm {

    public string $table = 'text_diffs';

    public int $id;
    /** standard mysql-compliant string uuid. */
    public string $diff_uuid;
    /** up to 64 chars, defined by the application using this library. */
    public string $source_uuid;
    /** actual diff operations */
    public string $diff;
    /** any extra information supplied by the application (256 chars) */
    public string $extra;
    /** short permissions-related string, defined by the application (32 chars) */
    public string $permission;
    /** When the diff occured. */
    public \DateTime $diff_time;

    public function set_from_db(array $row){
        $this->id = $row['id'];
        $this->diff_uuid = $this->bin_to_uuid($row['diff_uuid']);
        $this->source_uuid = $row['source_uuid'];
        $this->diff = $row['diff'];
        $this->extra = $row['extra'];
        $this->permission = $row['permission'];
        $this->diff_time = $this->str_to_datetime($row['diff_time']);
    }

    public function get_db_row(): array {
        $row = [
            'source_uuid' => $this->source_uuid,
            'diff'=>$this->diff,
            'extra'=>$this->extra,
            'permission'=>$this->permission
        ];
        if (isset($this->id))$row['id'] = $this->id;
        if (isset($this->source_uuid))$row['source_uuid'] = $this->source_uuid;
        if (isset($this->diff_time))$row['diff_time'] = $this->datetime_to_str($this->diff_time);

        return $row;
    }


}
