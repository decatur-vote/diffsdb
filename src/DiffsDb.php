<?php

namespace DecaturVote;

class DiffDb extends \Tlf\BigDb {

    protected string $orm_namespace = 'DecaturVote\\DiffDb';

    /**
     * Shorthand to create & store a diff
     *
     * @param $old_text
     * @param $new_text
     * @param $uuid uuid of the object being diffed
     * @param $permission a permission string relevant to your app. Your diff integration may use this. DiffDb simply stores this value.
     * @param $extra extra metadata string relevant to your app. Your diff integration may use this. DiffDb simply stores this value.
     *
     * @return the TextDiff orm object
     */
    public function store_diff(string $old_text, string $new_text, string $uuid, string $permission='', string $extra=''): \DecaturVote\DiffDb\TextDiff {
        $diff_ops = (new \DecaturVote\Differ())->get_opps($old_text, $new_text);

        $diff = new \DecaturVote\DiffDb\TextDiff($this);
        $diff->diff = $diff_ops;
        $diff->source_uuid = $uuid;
        $diff->permission = $permission;
        $diff->extra = $extra;
        $diff->save();
        $diff->refresh();
        return $diff;
    }

    /**
     * Update the 'permission' entry for all diffs of a given object uuid.
     *
     * @param $object_uuid
     * @param $new_permission
     *
     * @return int same as PDO::exec() PDO::exec() returns the number of rows that were modified or deleted by the SQL statement you issued. If no rows were affected, PDO::exec() returns 0. 
     */
    public function update_permissions(string $object_uuid, string $new_permission): int {
        return $this->exec('text_diffs.update_permissions', ['uuid'=>$object_uuid, 'permission'=>$new_permission]);
    }

    public function get_orm_class(string $table): string {
        if ($table=='text_diffs')return \DecaturVote\DiffDb\TextDiff::class;

        return parent::get_orm_class($table);
    }

    /**
     * Get an ordered array of diffs with the given permission & uuid
     * @param $uuid the uuid of the item diffs are for
     * @param $permission OPTIONAL permission to refine the query. If null, will return all diffs for the given uuid
     *
     * @return array<int, TextDiff> an array of textdiffs ordered from oldest (at index 0) to newest (at highest index)
     */
    public function get_diffs(string $uuid, ?string $permission = null): array{
        if ($permission==null){
            return $this->query('text_diffs', 'get', ['uuid'=>$uuid]);
        }

        return $this->query('text_diffs', 'get_with_permission', ['uuid'=>$uuid, 'permission'=>$permission]);
    }

    /**
     * Use diffs to convert the latest text to an older version of the text
     * @param $latest_text the text to convert with diffs
     * @param $diffs array<int TextDiff> diffs from oldest diff (at index 0) to newest diff (at highest index).
     *
     * @return the updated text after applying the diffs.
     */
    public function backward(string $latest_text, array $diffs): string {
        $diffs = array_reverse($diffs);
        $differ = new \DecaturVote\Differ();
        $text = $latest_text;
        foreach ($diffs as $d){
            $text = $differ->backward($text, $d->diff);
        }
        return $text;
    }

    /**
     * Use diffs to convert an old version of text to a newer version of text
     * @param $latest_text the text to convert with diffs
     * @param $diffs array<int TextDiff> diffs from oldest diff (at index 0) to newest diff (at highest index).
     *
     * @return the updated text after applying the diffs.
     */
    public function forward(string $old_text, array $diffs): string {
        $differ = new \DecaturVote\Differ();
        $text = $old_text;
        foreach ($diffs as $d){
            $text = $differ->forward($text, $d->diff);
        }
        return $text;
    }
}
