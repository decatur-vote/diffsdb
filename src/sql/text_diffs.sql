-- @query(create, -- END)
DROP TABLE IF EXISTS `text_diffs`;
CREATE TABLE IF NOT EXISTS `text_diffs` 
(
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `diff_uuid` binary(16) NOT NULL DEFAULT (UUID_TO_BIN( UUID() ) ),
    `source_uuid` VARCHAR(64) NOT NULL,
    `diff` TEXT NOT NULL,
    `extra` VARCHAR(256), -- for storing descriptive information, as needed by the application
    `permission` VARCHAR(32), -- intended for visibility (private, public, or other permission-refining method)

    `diff_time` DATETIME DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (`id`),
    UNIQUE(`diff_uuid`)

);
-- END

-- @query(get)
SELECT * FROM `text_diffs` WHERE `source_uuid` LIKE :uuid ORDER BY `diff_time` ASC;

-- @query(get_with_permission)
SELECT * FROM `text_diffs` WHERE `source_uuid` LIKE :uuid AND `permission` LIKE :permission ORDER BY `diff_time` ASC;

-- @query(update_permissions)
UPDATE `text_diffs` SET `permission` = :permission WHERE `source_uuid` LIKE :uuid;
