<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Diffs Db  
A simple database library for storing diffs and rewinding to previous versions (*or fast forwarding FROM old version*).   
  
Uses [`decatur-vote/text-diff`](https://gitlab.com/decatur-vote/text-diff) for generating diffs and [`taeluf/big-db`](https://gitlab.com/taeluf/php/big-db/) for database migrations, orm, and stored queries.  
  
For additional database & diff functionality, see the above libraries. DiffsDb is a very simple wrapper around them.  
  
## Status  
Probably ready to use? Not a lot of features (*it's supposed to be quite simple*). Tests passing (*though, tests are not very thorough*). Has not been tested in the wild.  
  
## Installation  
```bash  
composer require decatur-vote/diffs-db v1.0.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "decatur-vote/diffs-db": "v1.0.x-dev"}}  
```  
  
  
## Documentation  
- See **Usage** below for most use cases.  
- [TextDiff](/docs/api/src/orm/TextDiff.php.md) orm class  
- [DiffsDb](/docs/api/src/orm/DiffsDb.php.md) base class for the database layer (for BigDb). Contains the helper functions `store_diff`, and forward/backward.  
- [text_diffs.sql](/src/sql/text_diffs.sql) the sql used within this library  
- [Text Diff Library](https://gitlab.com/decatur-vote/text-diff) - for any manual text diff management  
- [BigDb Library](https://gitlab.com/taeluf/php/big-db/) - for more info about working with the database. You can always just use your PDO instance, too.  
  
## Initialize the Database  
Run this command in your CLI  
```bash  
bin/diffs-db create-db -pdo path/to/pdo.php  
```  
You MUST create the file `path/to/pdo.php` and have it return a PDO instance. The table will be created with that instance.  
  
## Usage  
This is basically everything.  
```php  
<?php  
$pdo = new \PDO(...);  
$diffDb = new \DecaturVote\DiffDb($pdo);  
$diffDb->migrate(0,1); // initialize the database  
  
// these 3 args are your responsibility, and can be used in your own custom queries however you wish.  
$uuid = uniqid(); // up to 64 chars  
$permission = 'public';  // up to 32 chars  
$extra = 'Descriptive text, not intended for machine use. permission is intended for machine uses'; // up to 256 chars  
  
// store_diff() returns a \DecaturVote\DiffDb\TextDiff object & INSERTs into database  
$diff1 = $diffDb->store_diff('original', 'first change', $uuid, $permission, $extra);  
$diff2 = $diffDb->store_diff('first change', "first change\nsecond change", $uuid, $permission, $extra);  
$diff3 = $diffDb->store_diff("first change\nsecond change",  "third change", $uuid, 'private', $extra);  
  
  
// get all diffs for the uuid.  
$diffs = $diffDb->get_diffs($uuid);  
// $diffDb->get_diffs($uuid, 'public'); # if you only want public diffs - Useful for displaying diffs to end-users, bad for converting text.  
  
// get your original text  
$original = $diffDb->backward("third change", $diffs);  
  
// use the original text + diffs to get the latest text  
$latest = $diffDb->forward('original', $diffs);  
  
// Only go back by one diff  
$back_one = $diffDb->backward("third change", array_slice($diffs,-1));  
```  
  
You can also use the [TextDiff](/src/orm/TextDiff.php) orm as you please. It's basically just properties for each database column.  
  
## Development Notes  
- Uses `taeluf/tester`. run tests with `phptest`  
- Uses `taeluf/code-scrawl`. Generate updated documentation with `vendor/bin/scrawl`. Edit docs in `.docsrc`, NOT in `docs/` or the root README.md  
