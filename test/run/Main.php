<?php

namespace DecaturVote\DiffDb\Test;

class Main extends \Tlf\Tester {

    public function getPdo($dbName = null): \PDO {
        $pdo = require($this->file('test/pdo.php'));

        return $pdo;
    }

    /**
     * 
     */
    public function testMain(){

        $pdo = $this->getPdo();
        $diffDb = new \DecaturVote\DiffDb($pdo);
        $diffDb->recompile_sql();
        $diffDb->migrate(0,1);

        $uuid = uniqid();
        $permission = 'public'; 
        $extra = 'Descriptive text, not intended for machine use. permission is intended for machine uses';

        $diff = $diffDb->store_diff($v0= 'original', $v1 = 'first change', $uuid, $permission, $extra);
        $diff = $diffDb->store_diff($v1, $v2= "first change\nsecond change", $uuid, $permission, $extra);
        $diff = $diffDb->store_diff($v2, $v3 = "third change", $uuid, 'private', $extra);

        // $latest_text = $v3;
        // $original_text = $v0;

        // all diffs for this uuid
        $diffs = $diffDb->get_diffs($uuid);

        $this->test("All the way backward");
        $original = $diffDb->backward($v3, $diffs);
        $this->compare($v0, $original);

        $this->test("All the way forward");
        $latest = $diffDb->forward($v0, $diffs);
        $this->compare($v3, $latest);

        $this->test("Backward one diff");
        $back_one = $diffDb->backward($v3, array_slice($diffs,-1));
        $this->compare($v2, $back_one);

        // @TODO text update_permissions()
    }
}

