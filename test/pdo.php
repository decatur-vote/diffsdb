<?php
/**
 * Return a PDO instance
 */


$file = __DIR__.'/../mysql_settings.json';
if (!file_exists($file)){
    echo "\n\n\n-----------\n\n";
    echo "You must create a file at $file with the keys 'database', 'user', 'password', and 'host' to run the tests on a mysql database.";
    echo "\n\n\n-----------\n\n";
    throw new \Exception("mysql_settings.json does not exist.");
}

$settings = json_decode(file_get_contents($file), true);
$pdo = new \PDO('mysql:dbname='.$settings['database'].';host='.$settings['host'],
    $settings['user'],$settings['password']);

return $pdo;
