<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/DiffsDb.php  
  
# class DecaturVote\DiffDb  
  
See source code at [/src/DiffsDb.php](/src/DiffsDb.php)  
  
## Constants  
  
## Properties  
- `protected string $orm_namespace = 'DecaturVote\\DiffDb';`   
  
## Methods   
- `public function store_diff(string $old_text, string $new_text, string $uuid, string $permission='', string $extra''): \DecaturVote\DiffDb\TextDiff` Shorthand to create & store a diff  
  
- `public function update_permissions(string $object_uuid, string $new_permission): int` Update the 'permission' entry for all diffs of a given object uuid.  
  
- `public function get_orm_class(string $table): string`   
- `public function get_diffs(string $uuid, string $permission = null): array` Get an ordered array of diffs with the given permission & uuid  
- `public function backward(string $latest_text, array $diffs): string` Use diffs to convert the latest text to an older version of the text  
- `public function forward(string $old_text, array $diffs): string` Use diffs to convert an old version of text to a newer version of text  
  
