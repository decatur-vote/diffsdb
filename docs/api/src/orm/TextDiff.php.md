<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/orm/TextDiff.php  
  
# class DecaturVote\DiffDb\TextDiff  
  
See source code at [/src/orm/TextDiff.php](/src/orm/TextDiff.php)  
  
## Constants  
  
## Properties  
- `public string $table = 'text_diffs';`   
- `public int $id;`   
- `public string $diff_uuid;` standard mysql-compliant string uuid.  
- `public string $source_uuid;` up to 64 chars, defined by the application using this library.  
- `public string $diff;` actual diff operations  
- `public string $extra;` any extra information supplied by the application (256 chars)  
- `public string $permission;` short permissions-related string, defined by the application (32 chars)  
- `public \DateTime $diff_time;` When the diff occured.  
  
## Methods   
- `public function set_from_db(array $row)`   
- `public function get_db_row(): array`   
  
